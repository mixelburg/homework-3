#pragma once
#include <iostream>
#include <ostream>

class Vector
{
	public:
		Vector(int n = 10);
		~Vector();
		int size() const;
		int capacity() const;
		int resizeFactor() const;
		bool is_empty() const;
		std::string elements_to_string() const;

		void push_back(const int& val);
		int pop_back();
		void reserve(int n);
		void resize(int n);
		void resize(int n, const int& val);
		void assign(int val) const;
		void delete_value(int val);

		Vector(const Vector& other);
		Vector& operator=(const Vector& other);
	
		int& operator[](int index) const;
		Vector& operator+=(const Vector& other);
		Vector& operator-=(const Vector& other);
		Vector operator+(const Vector& other) const; 
		Vector operator-(const Vector& other) const;

		friend std::ostream& operator<<(std::ostream& os, const Vector& v)
		{
			os << "Vector info: \n\tCapacity is " << v._capacity << "\n\tSize is " << v._size << "\n\tData is " << v.elements_to_string() << std::endl;
			return os;
		}


	private:
		int* _elements;
		int _size;
		int _capacity;
		int _resizeFactor;
};





