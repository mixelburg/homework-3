#include "Vector.h"
#include <iostream>
#include <string>

Vector::Vector(int n)
{
	if (n < 2)
		n = 2;

	this->_elements = new int[n];
	this->_resizeFactor = n;
	this->_capacity = n;
	this->_size = 0;
}

Vector::Vector(const Vector &other) 
{
	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;
	this->_size = other._size;

	this->_elements = new int[this->_capacity];

	std::copy(other._elements, other._elements + other.size(), this->_elements);
}

Vector::~Vector()
{
	delete[] this->_elements;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::is_empty() const
{
	return this->_size == 0;
}

std::string Vector::elements_to_string() const
{
	std::string elems;
	elems += "{";
	for (int i = 0; i < this->_size; i++)
	{
		elems += std::to_string(this->_elements[i]);

		if(i != this->_size - 1) elems += ", ";
	}
	elems += "}";
	return elems;
}

void Vector::push_back(const int& val)
{
	this->reserve(this->_size + 1);

	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	if (this->_size == 0)
	{
		std::cerr << "error: pop from empty vector";
		return -9999;
	}
	this->_size--;
	return this->_elements[this->_size];
}

void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		int new_size = this->_capacity + this->_resizeFactor * static_cast<int>((n - this->_capacity) / this->_resizeFactor);
		if (new_size <= this->_capacity) new_size += this->_resizeFactor;
		
		int* new_elements = new int[new_size];
		for (int i = 0; i < new_size; i++) new_elements[i] = -9999;
		for (int i = 0; i < this->_size; i++) new_elements[i] = this->_elements[i];
		
		delete[] this->_elements;
		this->_elements = new_elements;
		this->_capacity = n;
	}
}

void Vector::resize(int n)
{
	if (n <= this->_capacity)
		this->_size = n;
	else
	{
		this->reserve(n);
		this->_size = n;
	}
}

void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}

void Vector::assign(int val) const 
{
	for (int i = 0; i < this->_size; i++) this->_elements[i] = val;
}

void Vector::delete_value(const int val) 
{
	int new_size = 0;
	for (int i = 0; i < this->_size; i++)
		if (this->_elements[i] != val)
			new_size++;
	int* new_elements = new int[new_size];

	int cnt = 0;
	for (int i = 0; i < this->_size; i++)
		if (this->_elements[i] != val)
		{
			new_elements[cnt] = this->_elements[i];
			cnt++;
		}
			
	delete[] this->_elements;
	this->_elements = new_elements;
	this->_size = new_size;
}


//////////////// operators ////////////////
///
int& Vector::operator[](const int index) const
{
	if (index >= this->_size || index < 0)
	{
		std::cerr << "invalid index";
		exit(1);
	}
	return this->_elements[index];
}

Vector& Vector::operator+=(const Vector& other)
{
	if (this->_size != other._size)
	{
		std::cerr << "Can only perform operation on vectors with same length";
		exit(1);
	}
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] += other._elements[i];
	}
	return *this;
}

Vector& Vector::operator-=(const Vector& other)
{
	if (this->_size != other._size)
	{
		std::cerr << "Can only perform operation on vectors with same length";
		exit(1);
	}
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] -= other._elements[i];
	}
	return *this;
}

Vector Vector::operator+(const Vector& other) const
{
	if (this->_size != other._size)
	{
		std::cerr << "Can only perform operation on vectors with same length";
		exit(1);
	}
	
	Vector result = *this;
	for (int i = 0; i < result._size; i++)
	{
		result[i] += other._elements[i];
	}
	return result;
}

Vector Vector::operator-(const Vector& other) const
{
	if (this->_size != other._size)
	{
		std::cerr << "Can only perform operation on vectors with same length";
		exit(1);
	}

	Vector result = *this;
	for (int i = 0; i < result._size; i++)
	{
		result[i] -= other._elements[i];
	}
	return result;
}



//Vector Vector::operator+(const Vector& other) const
//{
//	Vector result = *this;
//	for (int i = 0; i < other._size; i++) result.push_back(other._elements[i]);
//	return result;
//}
//
//Vector Vector::operator-(const Vector& other) const
//{
//	Vector result = *this;
//
//	for (int i = 0; i < other._size; i++)
//	{
//		result.delete_value(other[i]);
//	}
//	return result;
//}
//
//
//Vector& Vector::operator+=(Vector other)
//{
//	this->reserve(this->_size + other._size);
//	for (int i = 0; i < other._size; i++) this->push_back(other._elements[i]);
//	
//	return *this;
//}
//
//Vector& Vector::operator-=(const Vector& other)
//{
//	for (int i = 0; i < other._size; i++)
//	{
//		this->delete_value(other[i]);
//	}
//	return *this;
//}
//



